/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teste_lapr1;

/**
 *
 * @author Zé
 */
public class biblio {
     public static void vetorProprioAproximado(double[][] matA, double[] vetorNormalizado) {
        double somaNormalizar = 0;
        for (int i = 0; i < matA.length; i++) {
            somaNormalizar = somaNormalizar + matA[i][0];
        }

        for (int j = 0; j < vetorNormalizado.length; j++) {
            vetorNormalizado[j] = (matA[j][0] / somaNormalizar);
        }

    }

    public static double[] vetorPrioridade(double[] vetorPrioridade, double[][] matA, double[] vetorNormalizado, double matComparacao[][]) {
        double a = 0, b = 0;

        for (int j = 0; j < matComparacao.length; j++) {
            b = 0;
            for (int i = 0; i < matComparacao.length; i++) {

                a = matComparacao[j][i] * vetorNormalizado[i];
                b = a + b;

            }
            vetorPrioridade[j] = b;
        }


        return vetorPrioridade;

    }

    public static double lambedaMax(double lambedaMax, double[] vetorPrioridade, double[][] matA, double[] vetorNormalizado, double matComparacao[][]) {
        double a = 0, b = 0, lambedaMaximo;

        for (int i = 0; i < matComparacao.length; i++) {

            b = vetorPrioridade[i] / vetorNormalizado[i];
            a = a + b;
        }
        lambedaMaximo = a / 3;
        return lambedaMaximo;
    }

    public static double indiceDeConsistencia(double lambedaMax, double[] vetorPrioridade, double[][] matA, double[] vetorNormalizado, double matComparacao[][]) {
        double indice = (lambedaMax - matComparacao.length) / (matComparacao.length - 1);
        return indice;

    }

    public static double razaoDeConsistencia(double indiceDeConsistencia, double lambedaMax, double[] vetorPrioridade, double[][] matA, double[] vetorNormalizado, double matComparacao[][]) {
        double razao = 0;
        razao = indiceDeConsistencia / 0.58;
        return razao;
    }

    //A partir daqui sao os Criterios - estilo, confiabilidade e consumo
    //Estilo
    public static double[] prioridadeRel(double mat[][]) {

        double[] soma = somaMat(mat);
//        for (int i = 0; i < 4; i++) {
//            System.out.println(somaEstilo[i]);
//        }
        double[][] normalizada = matNormalizada(mat, soma);
        return vetorProprioAprox(mat, soma, normalizada);

    }

    public static double[] somaMat(double mat[][]) {
        double[] soma = new double[mat.length];
        for (int i = 0; i < mat.length; i++) {
            double b = 0;
            for (int j = 0; j < mat.length; j++) {
                b = mat[j][i] + b;
            }
            soma[i] = b;
        }
        return soma;
    }

    public static double[][] matNormalizada(double mat[][], double[] soma) {
        double[][] normalizada = new double[mat.length][mat.length];
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat.length; j++) {
                normalizada[j][i] = mat[j][i] / soma[i];
            }
        }
//        for (int i = 0; i < matEstilo.length; i++) {
//            System.out.println(normalizadaEstilo[i][2]);
//        }
        return normalizada;
    }

    public static double[] vetorProprioAprox(double mat[][], double[] vecsoma, double[][] normalizada) {
        double[] proprioAprox = new double[mat.length];
        double soma = 0;
        for (int j = 0; j < mat.length; j++) {
            soma = 0;
            for (int i = 0; i < mat.length; i++) {
                soma = soma + normalizada[j][i];

            }
            proprioAprox[j] = soma / 4;

        }

        return proprioAprox;
    }
}
