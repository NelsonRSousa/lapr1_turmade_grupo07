package teste_lapr1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigDecimal;
import java.math.MathContext;
import java.text.ParseException;
import java.util.Scanner;
import java.util.Formatter;
import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;
import org.la4j.matrix.dense.Basic2DMatrix;
//import org.la4j.Matrhththix;
//import org.la4j.matrix.DenseMatrix;
//import org.la4j.matrix.dense.Basic2DMatrix;
//import org.la4j.decomposition.EigenDecompositor;

public class LAPR1 {

    public static final String FICHEIRO = "Output.txt";
    public static Scanner in = new Scanner(System.in);
    public static final int CASAS_DECIMAIS = 3;
    static String nomeFich = null;
//System.out.printf("%."+casasDecimais+"f",valor);
    private static int n_crit;
    private static int n_carros;

    static String[][] save = new String[100][100];

    public static void main(String[] args) throws FileNotFoundException, ParseException {
        int op;
        do {
            System.out.println("Através de que método pretende fazer a sua escolha?"
                    + "\n 1 - Método AHP"
                    + "\n 2 - Método TOPSIS"
                    + "\n 0 - Terminar");

            op = in.nextInt();
            switch (op) {
                case 1:
                    in.nextLine();
                    lerFicheiro();
                    double[][][] indMatrixCrit = new double[n_crit][n_carros][n_carros];
                    double[][][] indMatrixCritNorm = new double[n_crit][n_carros][n_carros];
                    double[] prioridadeRelativa = new double[n_crit];
                    double[][] prioridadeRelativaCrit = new double[n_crit][n_carros];
                    double[][] compar = new double[n_crit][n_crit];
                    double[][] comparNorm = new double[n_crit][n_crit];

                    double[] resultado = new double[n_crit];
                    double[][] resultadoCrit = new double[n_crit][n_carros];

                    double[] somaColunas = new double[n_crit];
                    double[][] somaColunasCrit = new double[n_crit][n_carros];
                    double[] bestChoice = new double[n_carros];
                    double[][] sort = new double[n_crit][n_carros];
                    separarMatrizComp(compar);
                    int cont = 0;
                    while (cont < n_crit) {
                        cont = separarMatrizCrit(indMatrixCrit, cont);
                    }
                    for (int i = 0; i < n_crit; i++) {
                        for (int j = 0; j < n_carros; j++) {
                            for (int k = 0; k < n_carros; k++) {
                                System.out.print(indMatrixCrit[i][j][k] + " ");
                            }
                            System.out.println("");
                        }
                        System.out.println("");
                    }
                    menu(compar, prioridadeRelativa, prioridadeRelativaCrit, indMatrixCrit, resultado, resultadoCrit, sort, bestChoice, indMatrixCritNorm, somaColunasCrit, comparNorm, somaColunas);
                    break;
                case 2:

                    break;
                case 0:
                    break;
                default:
                    System.out.println("Opção não suportada, tente novamente.");
            }
        } while (op != 0);

    }

    public static void menu(double[][] compar, double[] prioridadeRelativa, double[][] prioridadeRelativaCrit, double[][][] indMatrixCrit,
            double[] resultado, double[][] resultadoCrit, double[][] sort, double[] bestChoice,
            double[][][] indMatrixCritNorm, double[][] somaColunasCrit, double[][] comparNorm, double[] somaColunas) {
        int op;
        do {
            System.out.println("Deseja ir pelo método da biblioteca ou pelo método matemático?"
                    + "\n  1 - Biblioteca "
                    + "\n  2 - Matemático"
                    + "\n  0 - Sair");

            op = in.nextInt();
            switch (op) {

                case 1:
                    menuCaso1(compar, prioridadeRelativa, prioridadeRelativaCrit, indMatrixCrit, resultado, resultadoCrit, sort, bestChoice, comparNorm, indMatrixCritNorm);
                    break;
                case 2:
                    menuCaso2(indMatrixCrit, indMatrixCritNorm, somaColunasCrit,
                            compar, comparNorm, somaColunas, prioridadeRelativa, prioridadeRelativaCrit, resultado, resultadoCrit, sort, bestChoice);
                    break;
                case 0:
                    break;
                default:
                    System.out.println("Opção não suportada, tente novamente.");
            }
        } while (op != 0);
    }

    public static void menuCaso1(double[][] compar, double[] prioridadeRelativa, double[][] prioridadeRelativaCrit, double[][][] indMatrixCrit,
            double[] resultado, double[][] resultadoCrit, double[][] sort, double[] bestChoice, double[][] comparNorm, double[][][] indMatrixCritNorm) {

        Matrix a = new Basic2DMatrix(compar);

        //Obtem valores e vetores próprios fazendo "Eigen Decomposition"
        EigenDecompositor eigenD = new EigenDecompositor(a);

        Matrix[] mattD = eigenD.decompose();

        for (int i = 0; i < 2; i++) {
            System.out.println(mattD[i]);

        }

        // converte objeto Matrix (duas matrizes)  para array Java        
        double matA[][] = mattD[0].toDenseMatrix().toArray();

        for (int i = 0; i < matA.length; i++) {
            if (matA[i][0] < 0) {
                matA[i][0] = -matA[i][0];
            }

        }
        System.out.println("\n\n---------\nVetor Prioridade Comparação\n---------");
        double[] vetorPrioridade = new double[matA.length];
        double lambedaMax = 0;
        biblio.vetorProprioAproximado(matA, prioridadeRelativa);

        System.out.println("\n");
        for (int g = 0; g < prioridadeRelativa.length; g++) {
            System.out.println(prioridadeRelativa[g]);
        }
        System.out.println("\n\n---------\nVetor Prioridade Critérios\n---------");
        for (int i = 0; i < n_crit; i++) {
            prioridadeRelativaCrit[i] = biblio.prioridadeRel(indMatrixCrit[i]);
        }
        for (int i = 0; i < n_crit; i++) {
            for (int j = 0; j < n_carros; j++) {
                System.out.println(prioridadeRelativaCrit[i][j]);
            }
            System.out.println("");
        }
        biblio.vetorPrioridade(vetorPrioridade, matA, prioridadeRelativa, compar);
        lambedaMax = biblio.lambedaMax(lambedaMax, vetorPrioridade, matA, prioridadeRelativa, compar);
        System.out.println("\n---------\nLambeda Máximo\n---------\n" + lambedaMax);
        double indiceDeConsistencia = biblio.indiceDeConsistencia(lambedaMax, vetorPrioridade, matA, prioridadeRelativa, compar);
        System.out.println("\n---------\nIndice de consistência\n---------\n" + indiceDeConsistencia);
        double razaoDeConsistencia = biblio.razaoDeConsistencia(indiceDeConsistencia, lambedaMax, vetorPrioridade, matA, prioridadeRelativa, compar);
        System.out.println("\n---------\nRazão de consistência\n---------\n" + razaoDeConsistencia + "\n\n");

        mat.multiplicar(comparNorm, prioridadeRelativa, resultado);

        melhorEscolha(prioridadeRelativaCrit, prioridadeRelativa, bestChoice);

    }

    public static void menuCaso2(double[][][] indMatrixCrit, double[][][] indMatrixCritNorm, double[][] somaColunasCrit, double[][] compar,
            double[][] comparNorm, double[] somaColunas, double[] prioridadeRelativa, double[][] prioridadeRelativaCrit,
            double[] resultado, double[][] resultadoCrit, double[][] sort, double[] bestChoice) {
        for (int i = 0; i < n_crit; i++) {
            mat.normalizarMatrizCrit(indMatrixCrit, indMatrixCritNorm, i, somaColunasCrit);
        }
        mat.normalizarMatriz(compar, comparNorm, somaColunas);
        mat.prioridadeRelativa(comparNorm, prioridadeRelativa, n_crit);
        for (int i = 0; i < n_crit; i++) {
            mat.prioridadeRelativaCriterios(indMatrixCritNorm, i, prioridadeRelativaCrit, n_carros);
        }
        mat.multiplicar(compar, prioridadeRelativa, resultado);

        System.out.println("Matriz comparação normalizada");
        for (int i = 0; i < comparNorm.length; i++) {
            for (int j = 0; j < comparNorm[i].length; j++) {
                System.out.print(comparNorm[i][j] + " ");
            }
            System.out.println("");
        }
        System.out.println("\n");
        System.out.println("Matriz dos critérios normalizada");
        for (int i = 0; i < n_crit; i++) {
            for (int j = 0; j < n_carros; j++) {
                for (int k = 0; k < n_carros; k++) {
                    System.out.print(indMatrixCritNorm[i][j][k] + " ");
                }
                System.out.println("");
            }
            System.out.println("\n");
        }
        System.out.println("\n\n");

        System.out.println("\n");
        System.out.println("Vetor Prioridade Comparação");
        for (int i = 0; i < prioridadeRelativa.length; i++) {
            System.out.print(prioridadeRelativa[i] + "  ");
        }

        System.out.println("\n");
        System.out.println("Vetor Prioridade Critérios");
        for (int i = 0; i < n_crit; i++) {
            for (int j = 0; j < n_carros; j++) {

                System.out.print(prioridadeRelativaCrit[i][j] + "  ");
            }
            System.out.println("\n");
        }

        System.out.println("\n");
        System.out.println("Lambeda Máximo");
        mat.prioridadeRelativa(comparNorm, prioridadeRelativa, n_crit);
        mat.lambeda(compar, prioridadeRelativa, n_crit);
        System.out.println(mat.lambeda(compar, prioridadeRelativa, n_crit));
        System.out.println("\n");
        System.out.println("Indice de Consistência");
        mat.prioridadeRelativa(comparNorm, prioridadeRelativa, n_crit);
        mat.indiceConsistencia(mat.lambeda(compar, prioridadeRelativa, n_crit));
        System.out.println(mat.indiceConsistencia(mat.lambeda(compar, prioridadeRelativa, n_crit)));
        System.out.println("\n");
        System.out.println("Razão de Consistência");
        mat.prioridadeRelativa(comparNorm, prioridadeRelativa, n_crit);
        mat.razaoConsistencia(mat.indiceConsistencia(mat.lambeda(compar, prioridadeRelativa, n_crit)));
        System.out.println("\n");
        System.out.println("Melhor escolha");
        melhorEscolha(prioridadeRelativaCrit, prioridadeRelativa, bestChoice);

    }

    public static void lerFicheiro() throws FileNotFoundException {

        System.out.println("Introduza o nome do ficheiro:");
        nomeFich = in.nextLine();
        Scanner fichInput = new Scanner(new BufferedReader(new FileReader(nomeFich)));
        //Scanner fichInput = new Scanner(new File(nomeFich));
        int n = 0;
        while (fichInput.hasNext()) {

            String linha = fichInput.nextLine();
            n = converterFicheiroMatriz(linha, n);
        }

        fichInput.close();
        System.out.println("Ficheiro carregado com sucesso.");

    }

    private static int converterFicheiroMatriz(String linha, int n) {

        String[] temp = linha.split("\\s+");
        if (temp.length == 0) {

        } else if (temp[0].contains("mc_criterios")) { // linhas que contem o cabeçalho de cada matriz
            n_crit = temp.length - 1;

        } else if (temp[0].contains("mcp_")) {
            n_carros = temp.length - 1;
        } else if (linha.contains("1")) { // cada linha contem pelo menos o numero "1"
            for (int i = 0; i < temp.length; i++) {
                save[n][i] = temp[i];
            }
            n++;
        }
        return n;
    }

    
    
    private static void separarMatrizComp(double[][] compar) throws ParseException {
        for (int i = 0; i < n_crit; i++) {
            for (int j = 0; j < n_crit; j++) {
                compar[i][j] = mat.fractionToDouble(save[i][j]);
            }
        }
    }

    private static int separarMatrizCrit(double[][][] indMatrixCrit, int cont) throws ParseException {
        for (int i = 0; i < n_carros; i++) {
            for (int j = 0; j < n_carros; j++) {
                indMatrixCrit[cont][i][j] = mat.fractionToDouble(save[(cont * n_carros) + n_crit + i][j]);
            }
        }
        return cont + 1;
    }

    public static void melhorEscolha(double[][] prioridadeRelativaCrit, double[] prioridadeRelativa, double[] bestChoice) {
        double soma;
        for (int i = 0; i < n_carros; i++) {
            soma = 0;
            for (int j = 0; j < n_crit; j++) {
                soma += (prioridadeRelativaCrit[j][i] * prioridadeRelativa[j]);
            }
            bestChoice[i] = soma;
            System.out.println("Opção " + (i + 1) + ": " + soma + "\n");
        }
        double maior = 0;
        for (int i = 0; i < bestChoice.length; i++) {
            if (bestChoice[i] > maior) {
                maior = bestChoice[i];
            }
        }
        for (int i = 0; i < n_carros; i++) {
            if (bestChoice[i] == maior) {
                System.out.println("Uma/A melhor escolha é a opção " + (i + 1));
            }
        }
    }
}
