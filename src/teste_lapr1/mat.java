package teste_lapr1;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.ParseException;

public class mat {

    private final static int N = 1;


    private final static int CASAS_DECIMAIS = 4;

    private final static double IR = 0.58;

    public static void normalizarMatrizCrit(double[][][] matriz, double[][][] matrizNorm, int index, double[][] somaMatriz) {
        somaColunaCrit(matriz, index, somaMatriz);
        for (int i = 0; i < matriz[index].length; i++) {
            for (int j = 0; j < matriz[index][i].length; j++) {
                matrizNorm[index][j][i] = matriz[index][j][i] / somaMatriz[index][i];
            }
        }
    }

    public static void somaColunaCrit(double[][][] matriz, int index, double somaColunas[][]) {
        for (int i = 0; i < matriz[index].length; i++) {
            for (int j = 0; j < matriz[index][i].length; j++) {
                somaColunas[index][i] = somaColunas[index][i] + matriz[index][j][i];
            }
        }
    }

    public static void normalizarMatriz(double[][] matriz, double[][] matrizNorm, double[] somaMatriz) {
        somaColuna(matriz, somaMatriz);
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                matrizNorm[j][i] = matriz[j][i] / somaMatriz[i];
            }
        }
    }

    public static void somaColuna(double[][] matriz, double somaColunas[]) {
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                somaColunas[i] = somaColunas[i] + matriz[j][i];
            }
        }
    }

    public static void prioridadeRelativa(double[][] matriz, double[] prioridadeRelativa,int n_crit) {
        double temp = 0;
        for (int i = 0; i < matriz.length; i++) {
            double soma = 0;
            for (int j = 0; j < matriz[i].length; j++) {
                soma = soma + matriz[i][j]; // soma da linha 
            }
            temp = soma / n_crit;
            prioridadeRelativa[i] = arredondar(CASAS_DECIMAIS, temp);
        }

    }

    public static void prioridadeRelativaCriterios(double[][][] matriz,int index, double[][] prioridadeRelativa,int n_carros) {

        for (int i = 0; i < matriz[index].length; i++) {
            double soma = 0;
            for (int j = 0; j < matriz[index][i].length; j++) {
                soma = soma + matriz[index][i][j];
            }
            double temp = soma / n_carros;
            prioridadeRelativa[index][i] = arredondar(CASAS_DECIMAIS, temp);  //arrendoda os valores com 4 casas decimais
        }

    }

    public static void multiplicar(double[][] norm, double[] vetorPrioridade, double[] resultado) {

        double soma;
        if (norm[0].length == vetorPrioridade.length) {
            for (int i = 0; i < norm.length; i++) {
                soma = 0;
                for (int j = 0; j < vetorPrioridade.length; j++) {
                    soma = soma + (norm[i][j] * vetorPrioridade[j]);
                }
                resultado[i] = soma;
            }
        } else {
            System.out.println("Operação de matrizes inválida!");
        }
    }
    
    public static void multiplicarCrit(double[][][] norm,int index, double[][] vetorPrioridade, double[][] resultado) {

        double soma;
        if (norm[0].length == vetorPrioridade.length) {
            for (int i = 0; i < norm.length; i++) {
                soma = 0;
                for (int j = 0; j < vetorPrioridade.length; j++) {
                    soma = soma + (norm[index][i][j] * vetorPrioridade[index][j]);
                }
                resultado[index][i] = soma;
            }
        } else {
            System.out.println("Operação de matrizes inválida!");
        }
    }

    public static double arredondar(int CasasDecimais, double valor) {
        int temp = 1;
        for (int i = 0; i < CasasDecimais; i++) {
            temp = temp * 10;
        }
        valor = valor * temp;
        valor = Math.round(valor);
        valor = valor / temp;
        return valor;

    }

    public static double lambeda(double[][] comp, double[] vetorPrioridade,int n_crit) {
        double soma;
        double soma1;
        double lam;
        double lambeda;
        double resultado2[] = new double[n_crit];
        double resultado3[] = new double[n_crit];
        for (int i = 0; i < comp.length; i++) {
            soma = 0;
            for (int j = 0; j < comp[i].length; j++) {
                soma += comp[i][j] * vetorPrioridade[j];
            }
            resultado2[i] = arredondar(CASAS_DECIMAIS, soma);

        }
//        for (int l = 0; l < N_CRIT; l++) {
//            System.out.println(resultado2[l]);
//            
//        }         
//    System.out.println("\n\n");

        for (int m = 0; m < resultado2.length; m++) {
            soma1 = 0;

            soma1 += resultado2[m] / vetorPrioridade[m];

            resultado3[m] = arredondar(CASAS_DECIMAIS, soma1);

        }
//         for (int a = 0; a < N_CRIT; a++) {
//            System.out.println(resultado3[a]);
//            
//         }
//         System.out.println("\n\n");
        lam = 0;
        lambeda = 0;
        for (int o = 0; o < resultado3.length; o++) {
            lam = lam + resultado3[o];
            lambeda = lam / 3;

        }

        return arredondar(CASAS_DECIMAIS, lambeda);
    }

    public static double indiceConsistencia(double lambeda) {

        double ic = (lambeda - 3) / (3 - 1);
        arredondar(CASAS_DECIMAIS, ic);

        return arredondar(CASAS_DECIMAIS, ic);
    }

    public static void razaoConsistencia(double indiceConsistencia) {
        double rc = 0;
        rc = indiceConsistencia / 0.58;

        System.out.println(arredondar(CASAS_DECIMAIS, rc));
    }

    public static int[][] calculaProduto(int[][] a, int[][] b) {

        if (a[0].length != b.length) {
            throw new RuntimeException("Dimensões inconsistentes. Impossível multiplicar as matrizes");
        }

        int[][] result = new int[a.length][b[0].length];

        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < b[0].length; j++) {
                for (int k = 0; k < a[0].length; k++) {
                    result[i][j] += (a[i][k] * b[k][j]);
                }
            }
        }
        return result;
    }

    public static Double fractionToDouble(String fraction) throws ParseException {
        Double d = null;
        if (fraction != null) {
            if (fraction.contains("/")) {
                String[] numbers = fraction.split("/");
                if (numbers.length == 2) {
                    BigDecimal d1 = BigDecimal.valueOf(Double.valueOf(numbers[0]));
                    BigDecimal d2 = BigDecimal.valueOf(Double.valueOf(numbers[1]));
                    BigDecimal response = d1.divide(d2, MathContext.DECIMAL128);
                    d = response.doubleValue();
                }
            } else {
                d = Double.valueOf(fraction);
            }
        }
        if (d == null) {
            throw new ParseException(fraction, 0);
        }
        return d;
    }

//    public static void selecaoMelhorAlternativa(double[] prioridadeComposta, String[] nomeAlternativas, Formatter fOut) {
//        double max = prioridadeComposta[0];
//        int posicao = 0;
//        for (int i = 1; i < prioridadeComposta.length; i++) {
//            if (prioridadeComposta[i] > max) {
//                posicao = i;
//            }
//        }
//        System.out.println("A melhor alternativa é a opção " + nomeAlternativas[posicao]);
//        fOut.format("%n");
//        fOut.format("A melhor alternativa é a opção " + nomeAlternativas[posicao]);
//    }
    // public static void multiplicar1(double[][] comp, double[] vetorPrioridade, double[] resultado) {
//        double soma;
//        for (int i = 0; i < comp.length; i++) {
//            soma = 0;
//            for (int j = 0; j < comp[i].length; j++) {
//                soma += comp[i][j] * vetorPrioridade[j];
//            }
//            resultado[i] = arredondar(CASAS_DECIMAIS, soma);
//
//        }
//        for (int l = 0; l < N_CARROS; l++) {
//            System.out.println(resultado[l]);
//        }
//
//    }
}
